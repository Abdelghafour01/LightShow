import Vue from 'vue';
import Router from 'vue-router';

import Dashboard from '@/components/Dashboard';

import Amplitude from '@/components/Light/Amplitude';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Dashboard
        },
        {
            path: '/amplitude',
            component: Amplitude
        }
    ]
})
